//
//  AppDelegate.h
//  p01-nadkarni
//
//  Created by Deepak Dinesh Gupta on 01/02/16.
//  Copyright © 2016 tnadkar2. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

