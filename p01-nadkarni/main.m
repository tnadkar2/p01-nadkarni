//
//  main.m
//  p01-nadkarni
//
//  Created by Deepak Dinesh Gupta on 01/02/16.
//  Copyright © 2016 tnadkar2. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
